from django.contrib import admin

# Register your models here.
from blog.models import *

class BlogInline(admin.TabularInline):
	model = comments
	extra = 2


class BlogAdmin(admin.ModelAdmin):
	fields = ['blog_text', 'blog_image', 'blog_pub_date','blog_title',]
	list_filter = ['blog_pub_date']
	inlines = [
	BlogInline,
	]


admin.site.register(blog, BlogAdmin)