# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-06-06 21:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0004_auto_20160603_2232'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comments',
            name='comments_blog',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, to='blog.blog'),
        ),
    ]
