# -*- coding: UTF-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class blog(models.Model):
    blog_title = models.CharField('Заголовок',max_length = 200)
    blog_text = models.TextField('Статья', blank = True, null = True)
    blog_image = models.ImageField('Фото', upload_to = "blog/static/", blank = True, null = True)
    blog_pub_date = models.DateTimeField('Дата публикации', blank = True, null = True)
    blog_likes = models.IntegerField('Лайки', blank = True, null = True)

    class Meta:
        verbose_name = "Статья"
        verbose_name_plural = "Статьи"
        db_table = 'blog'

    def _get_img(self):
        return str(self.blog_image.url.split("/")[-1])
    img = property(_get_img)

    def _get_short_text(self):
        if len(self.blog_text) > 100:
            return "{}...".format(self.blog_text[:100])
        return self.blog_text
    short_text = property(_get_short_text)
    
    def __unicode__(self):
        return self.blog_title


class comments(models.Model):
    class Meta():
        db_table = 'comments'
    comments_text = models.TextField(blank = True, null = True)
    comments_blog = models.ForeignKey(blog, blank = True)
    
    def __unicode__(self):
        return self.comments_text
