from django.conf.urls import patterns, include, url
from django.contrib import admin
from blog import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    url(r'^', views.blog_views, name = 'blog'),
    url(r'^(\d+)/$', views.blog_view, name = 'page'),
    #url(r'^blog/', include('blog.urls')),
    

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
